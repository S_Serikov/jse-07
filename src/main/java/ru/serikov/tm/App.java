package ru.serikov.tm;

import ru.serikov.tm.dao.ProjectDAO;
import ru.serikov.tm.dao.TaskDAO;

import java.util.Scanner;

import static ru.serikov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private static ProjectDAO projectDAO = new ProjectDAO();

    private static TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        process();
    }

    public static void process() {
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    private static void run (final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int  result = run(param);
        System.exit(result);
    }

    private static int run (final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case CMD_VERSION: return displayVersion();
            case CMD_ABOUT: return displayAbout();
            case CMD_HELP: return displayHelp ();
            case CMD_EXIT: return Exit ();

            case CMD_PROJECT_CREATE: return createProject();
            case CMD_PROJECT_CLEAR: return clearProject();
            case CMD_PROJECT_LIST: return listProject();

            case CMD_TASK_CREATE: return createTask();
            case CMD_TASK_CLEAR: return clearTask();
            case CMD_TASK_LIST: return listTask();

            default: return displayError();
        }
    }

    private static int createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME");
        final String name = scanner.nextLine();
        projectDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearProject(){
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listProject(){
        System.out.println("[LIST PROJECT]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private static int createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME");
        final String name = scanner.nextLine();
        taskDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearTask(){
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listTask(){
        System.out.println("[LIST TASK]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private static int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static int displayHelp() {
        System.out.println("version - display application version");
        System.out.println("about - display developer info");
        System.out.println("help - display list of commands");
        System.out.println("exit - terminate console application");
        System.out.println();
        System.out.println("project-create - create new project by name");
        System.out.println("project-clear - remove all project");
        System.out.println("project-list - display list of projects");
        System.out.println();
        System.out.println("task-create - create new task by name");
        System.out.println("task-clear - remove all task");
        System.out.println("task-list - display list of tasks");
        return 0;
    }

    private static int displayAbout() {
        System.out.println("Sergey Serikov");
        System.out.println("serikov_sy@nlmk.com");
        return 0;
    }

    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    private static int Exit() {
        System.exit(0);
        return 0;
    }

}
